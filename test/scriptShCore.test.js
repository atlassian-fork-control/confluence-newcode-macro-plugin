/**
 * A Test SyntaxHighligher regular expression that process invalid url to prevent XSS.
 * Strip onclick event from the input url
 * This is test against minified file scripts/shCore.js
 */
const scriptShCore = require('../src/main/resources/sh/scripts/shCore');

test('regex invalid url', () => {
    expect(scriptShCore.SyntaxHighlighter.Highlighter.prototype.processUrls('http://google.com/"onclick=alert(document.cookie)//'))
.toBe('<a href="http://google.com/">http://google.com/</a>"onclick=alert(document.cookie)//')
});

test('regex invalid url with escape', () => {
    expect(scriptShCore.SyntaxHighlighter.Highlighter.prototype.processUrls('http://google.com/%20onclick=alert(document.cookie)//'))
.toBe('<a href="http://google.com/">http://google.com/</a>%20onclick=alert(document.cookie)//')
});

test('regex invalid url with escaped angle bracket', () => {
    expect(scriptShCore.SyntaxHighlighter.Highlighter.prototype.processUrls('&lt;http://google.com/"onclick=alert(document.cookie)//&gt;'))
.toBe('&lt;<a href="http://google.com/">http://google.com/</a>"onclick=alert(document.cookie)//&gt;')
});

test('regex valid url with escaped angle bracket', () => {
    expect(scriptShCore.SyntaxHighlighter.Highlighter.prototype.processUrls('&lt;http://google.com/&gt;'))
.toBe('&lt;<a href="http://google.com/">http://google.com/</a>&gt;')
});