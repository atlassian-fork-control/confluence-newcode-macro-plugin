package com.atlassian.confluence.ext.code.render;

import com.atlassian.confluence.ext.code.config.NewcodeSettings;

import java.util.Map;

import static com.atlassian.confluence.ext.code.render.Parameters.DEFAULT_PARAMETER;
import static com.atlassian.confluence.ext.code.render.Parameters.PARAM_LANG;
import static com.atlassian.confluence.ext.code.render.Parameters.PARAM_LANGUAGE;

/**
 * Parameter implementation of the language parameter.
 */
public final class LanguageParameter extends MappedParameter {

    private static final Object NONE_LANG = "none";
    private static final String PLAIN_LANG = "plain";
    private static final String AS_LANG = "actionscript";
    private static final String AS3_LANG = "actionscript3";

    /**
     * Default constructor.
     *
     * @param name       The name used by the macro.
     * @param mappedName The name used by the syntax highlighter
     */
    public LanguageParameter(final String name, final String mappedName) {
        super(name, mappedName);
    }

    /**
     * Determines the language to use.
     *
     * @param parameters The map of parameters.
     * @return The language to use.
     */
    @Override
    public String getValue(final Map<String, String> parameters) {
        String lang = parameters.get(PARAM_LANG) == null ?
                parameters.get(PARAM_LANGUAGE) : parameters.get(PARAM_LANG);
        if (lang == null) {
            lang = parameters.get(DEFAULT_PARAMETER);
            if (lang == null) {
                lang = NewcodeSettings.DEFAULT_LANGUAGE_VALUE.toLowerCase();
            }
        }

        if (NONE_LANG.equals(lang)) {
            lang = PLAIN_LANG;
        }

        if (AS_LANG.equals(lang)) {
            lang = AS3_LANG;
        }
        return lang.toLowerCase();
    }

}
