package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.languages.Language;

import java.util.Collection;

/**
 * Represent languages which are registered externally with this macro.
 */
public final class RegisteredLanguage implements Language {

    private String name;
    private String friendlyName;
    private Collection<String> aliases;
    private String webResource;

    /**
     * Default constructor.
     *
     * @param name    The name of the language
     * @param aliases The aliases for this language
     */
    public RegisteredLanguage(final String name,
                              final Collection<String> aliases,
                              final String friendlyName) {
        super();
        this.name = name;
        this.aliases = aliases;
        this.friendlyName = friendlyName;
    }

    /**
     * {@inheritDoc}
     */
    public Collection<String> getAliases() {
        return aliases;
    }

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isBuiltIn() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public String getWebResource() {
        return webResource;
    }

    public void setWebResource(String webResource) {
        this.webResource = webResource;
    }

    public String getFriendlyName() {
        return friendlyName;
    }
}
