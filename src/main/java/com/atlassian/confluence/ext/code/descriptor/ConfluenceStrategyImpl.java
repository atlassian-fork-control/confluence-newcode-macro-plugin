package com.atlassian.confluence.ext.code.descriptor;

import com.atlassian.confluence.ext.code.util.Constants;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * {@link ConfluenceStrategy} compatible with Confluence 2.10 and higher.
 */
@Component
public class ConfluenceStrategyImpl implements ConfluenceStrategy {

    private static final String THEME_DESCRIPTOR_PREFIX = "sh-theme-";
    private static final String LAYOUT_PREFIX = "layout-";
    private static final String LOCALIZATION_DESCRIPTOR_PREFIX = "syntaxhighlighter-lang-";

    private final PluginAccessor pluginAccessor;

    @Autowired
    public ConfluenceStrategyImpl(final @ComponentImport PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    /**
     * {@inheritDoc}
     */
    public BrushDefinition[] listBuiltinBrushes() {
        ModuleDescriptor<?> descriptor = getDescriptor("syntaxhighlighter-brushes");
        List<ResourceDescriptor> resources = descriptor.getResourceDescriptors();
        BrushDefinition[] result = new BrushDefinition[resources.size()];
        int i = 0;
        for (ResourceDescriptor resource : resources) {
            result[i] = new BrushDefinition(resource.getLocation(), descriptor.getCompleteKey());
            i++;
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public ThemeDefinition[] listBuiltinThemes() {
        Plugin plugin = pluginAccessor.getPlugin(Constants.PLUGIN_KEY);
        Collection<ModuleDescriptor<?>> descriptors = plugin.getModuleDescriptors();

        List<ThemeDefinition> result = new ArrayList<>();
        for (ModuleDescriptor<?> descriptor : descriptors) {
            if (descriptor.getKey().startsWith(THEME_DESCRIPTOR_PREFIX)) {
                List<ResourceDescriptor> resources = descriptor.getResourceDescriptors();
                ResourceDescriptor first = resources.get(0);
                String location = first.getLocation();
                String webResourceId = descriptor.getCompleteKey();

                // Get panel look and feel
                Map<String, String> panelLookAndFeel = new HashMap<>();
                for (Map.Entry<String, String> param : descriptor.getParams().entrySet()) {
                    if (param.getKey().startsWith(LAYOUT_PREFIX)) {
                        panelLookAndFeel.put(param.getKey(), param.getValue());
                    }
                }

                result.add(new ThemeDefinition(location, webResourceId, panelLookAndFeel));
            }
        }

        return result.toArray(new ThemeDefinition[result.size()]);
    }

    public List<String> listLocalization() {
        Plugin plugin = pluginAccessor.getPlugin(Constants.PLUGIN_KEY);
        Collection<ModuleDescriptor<?>> descriptors = plugin.getModuleDescriptors();

        List<String> result = new ArrayList<>();
        for (ModuleDescriptor<?> descriptor : descriptors) {
            if (descriptor.getKey().startsWith(LOCALIZATION_DESCRIPTOR_PREFIX)) {
                String webResourceId = descriptor.getCompleteKey();
                if (StringUtils.isNotEmpty(webResourceId)) {
                    String languageKey = webResourceId.substring(webResourceId.length() - 2,
                            webResourceId.length());
                    result.add(languageKey.toLowerCase());
                }
            }
        }

        return result;
    }

    /**
     * Returns a module descriptor identified by key.
     *
     * @param key The key of the module descriptor
     * @return The module descriptor
     */
    protected ModuleDescriptor<?> getDescriptor(final String key) {
        return pluginAccessor.getPlugin(Constants.PLUGIN_KEY)
                .getModuleDescriptor(key);
    }
}
