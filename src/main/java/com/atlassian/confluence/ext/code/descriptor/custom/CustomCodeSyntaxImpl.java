package com.atlassian.confluence.ext.code.descriptor.custom;

import com.atlassian.plugin.Plugin;
import org.dom4j.Element;

/**
 * Provides a concrete implementation of the {@link CustomCodeSyntax} plugin module interface.
 */
class CustomCodeSyntaxImpl implements CustomCodeSyntax {
    private final String resourceKey;
    private final String friendlyName;

    /**
     * Constructs a new instance - should only be called by
     * {@link CustomCodeSyntaxModuleDescriptor#init(Plugin, Element)}
     */
    CustomCodeSyntaxImpl(final String resourceKey, String friendlyName) {
        this.resourceKey = resourceKey;
        this.friendlyName = friendlyName;
    }

    /**
     * {@inheritDoc}
     */
    public String getResourceKey() {
        return resourceKey;
    }

    /**
     * {@inheritDoc}
     */
    public String getFriendlyName() {
        return friendlyName;
    }
}
