package com.atlassian.confluence.ext.code.languages;

import java.util.Collection;

/**
 * Interface for the registration of supported languages.
 */
public interface Language {

    String getFriendlyName();

    /**
     * @return The name of the language
     */
    String getName();

    /**
     * @return The aliases for the language
     */
    Collection<String> getAliases();

    /**
     * @return whether the language is part of the macro or external
     */
    boolean isBuiltIn();

    /**
     * @return the id of the web resource to include for this language
     */
    String getWebResource();
}
