package com.atlassian.confluence.ext.code.render;

import java.util.Map;

/**
 * Base class for parameters of which the values and/or names have to be
 * translated between those understood by the macro and those understood by the
 * syntax highlighter library.
 */
interface Parameter {

    /**
     * Returns the name of the parameter as understood by the syntax
     * highlighter.
     *
     * @return the name of the parameter as understood by the syntax highlighter
     */
    String getName();

    /**
     * Returns the name of the parameter as understood by the macro.
     *
     * @return The name of the parameter as understood by the macro
     */
    String getMacroName();

    /**
     * Returns the value of the parameter as understood by the syntax
     * highlighter library. This method may take default values, or dependent
     * values into account while trying to determine the value for the
     * parameter.
     *
     * @param parameters The map of macro parameters
     * @return The value for this parameter
     * @throws InvalidValueException In case of invalid values
     */
    String getValue(Map<String, String> parameters)
            throws InvalidValueException;

}
