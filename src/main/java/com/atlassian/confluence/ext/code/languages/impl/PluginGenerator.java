package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.plugin.PluginArtifact;

import java.io.IOException;
import java.io.Reader;

public interface PluginGenerator {
    PluginArtifact createPluginForLanguage(final Language languageToRegister,
                                           final Reader scriptContents) throws IOException;
}
