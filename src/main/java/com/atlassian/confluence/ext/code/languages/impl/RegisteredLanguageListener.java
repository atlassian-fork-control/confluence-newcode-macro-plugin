package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.descriptor.custom.CustomCodeSyntax;
import com.atlassian.confluence.ext.code.descriptor.custom.CustomCodeSyntaxModuleDescriptor;
import com.atlassian.confluence.ext.code.languages.DuplicateLanguageException;
import com.atlassian.confluence.ext.code.languages.InvalidLanguageException;
import com.atlassian.confluence.ext.code.languages.LanguageParser;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * Adds instances of {@link RegisteredLanguage} to the {@link LanguageRegistry} by tracking the
 * Plugin Framework for added and removed instances of {@link CustomCodeSyntaxModuleDescriptor}.
 * Relies on the plugin framework's awesome {@link PluginModuleTracker} helper to keep
 * synchronicity with the state of the plugin framework.
 */
@Component
public class RegisteredLanguageListener implements DisposableBean {
    private static final Logger log = LoggerFactory.getLogger(RegisteredLanguageListener.class);

    private final LanguageRegistry languageRegistry;
    private final LanguageParser languageParser;
    private final PluginModuleTracker<CustomCodeSyntax, CustomCodeSyntaxModuleDescriptor> moduleTracker;

    @Autowired
    public RegisteredLanguageListener(final LanguageRegistry languageRegistry,
                                      final @ComponentImport PluginAccessor pluginAccessor,
                                      final @ComponentImport PluginEventManager pluginEventManager,
                                      final LanguageParser languageParser) {
        this.languageRegistry = languageRegistry;
        this.languageParser = languageParser;
        this.moduleTracker = new DefaultPluginModuleTracker<>(pluginAccessor, pluginEventManager,
                CustomCodeSyntaxModuleDescriptor.class, new Customizer());
    }

    /**
     * Custom callbacks that get invoked when the @{link PluginModuleTracker} detects a change in plugin module state
     * that should be acted upon.
     */
    private class Customizer implements PluginModuleTracker.Customizer<CustomCodeSyntax, CustomCodeSyntaxModuleDescriptor> {
        // Maintain a map of plugin module key -> code language so that if/when the plugin module
        // gets un-installed or disabled, we know which Languages we need to un-install from the
        // LanguageRegistry without having the parse the language script again.
        private final Map<String, String> moduleKeyMap = Maps.newHashMap();

        /**
         * When a {@link CustomCodeSyntaxModuleDescriptor} is enabled, parses the JavaScript file from the associated
         * web-resource module and registers it as a custom syntax highlighter for the code macro (assuming the JavaScript
         * is valid)
         *
         * @param descriptor The enabled plugin module descriptor.
         * @return The enabled plugin module descriptor.
         */
        public CustomCodeSyntaxModuleDescriptor adding(final CustomCodeSyntaxModuleDescriptor descriptor) {
            log.debug(String.format("Handling registration of new " +
                    "CustomCodeSyntaxModuleDescriptor %s", descriptor.getCompleteKey()));
            CustomCodeSyntax module = descriptor.getModule();
            Plugin otherPlugin = descriptor.getPlugin();

            // Identify the related WebResource module that contains the related JavaScript language file.
            ModuleDescriptor<?> moduleDescriptor = otherPlugin.getModuleDescriptor(module.getResourceKey());
            if (!(moduleDescriptor instanceof WebResourceModuleDescriptor)) {
                log.error("Failed to register new code macro language " +
                        descriptor.getCompleteKey() + " because its related resource key was not a web-resource plugin module.");
                return descriptor;
            }

            // Grab the first resource in this module and assume it is the language file.
            List<ResourceDescriptor> resourceDescriptors = moduleDescriptor.getResourceDescriptors();
            if (resourceDescriptors.size() == 0) {
                log.error("Failed to register new code macro language " +
                        descriptor.getCompleteKey() + " because its related web-resource module "
                        + moduleDescriptor.getCompleteKey() + " had no declared resources.");
                return descriptor;
            } else if (resourceDescriptors.size() > 1) {
                log.warn("More than one declared resource found in web-resource module " +
                        moduleDescriptor.getCompleteKey() + " a single resource will be selected " +
                        "non-deterministically.");
                return descriptor;
            }
            ResourceDescriptor resourceDescriptor = resourceDescriptors.get(0);

            // Read in and parse the language file
            ClassLoader classLoader = otherPlugin.getClassLoader();
            InputStream brushFile = null;
            try {
                brushFile = classLoader.getResourceAsStream(resourceDescriptor.getLocation());
                InputStreamReader reader;
                try {
                    reader = new InputStreamReader(brushFile, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    log.error("Failed to read input stream from code module in plugin " +
                            otherPlugin.getKey() + ": " + e.getMessage(), e);
                    return descriptor;
                }

                RegisteredLanguage language;
                try {
                    language = languageParser.parseRegisteredLanguage(reader,
                            module.getFriendlyName());
                    language.setWebResource(moduleDescriptor.getCompleteKey());
                } catch (InvalidLanguageException e) {
                    log.error("Language file from plugin " + otherPlugin.getKey() +
                            " was invalid. Skipping. " + e.getMessage(), e);
                    return descriptor;
                }

                try {
                    languageRegistry.addLanguage(language);
                    moduleKeyMap.put(descriptor.getCompleteKey(), language.getName());
                } catch (DuplicateLanguageException e) {
                    log.error("Failed to register new language " + language.getName() +
                            " it or one of its aliases is already registered: " + e.getMessage(), e);
                    return descriptor;
                }
            } finally {
                if (brushFile != null)
                    IOUtils.closeQuietly(brushFile);
            }

            return descriptor;
        }

        public void removed(CustomCodeSyntaxModuleDescriptor descriptor) {
            log.debug("Handling possible code syntax removal for disabled plugin module " + descriptor.getCompleteKey());
            String pluginModuleKey = descriptor.getCompleteKey();
            String languageKey = moduleKeyMap.get(pluginModuleKey);

            if (!StringUtils.isBlank(languageKey)) {
                log.info("Removing registered language " + languageKey);
                languageRegistry.unregisterLanguage(languageKey);
                moduleKeyMap.remove(pluginModuleKey);
            }
        }
    }

    public void destroy() {
        // The Module Tracker hooks in to the event publishing system. Therefore, we need to manually un-link it when
        // shutting down this plugin or else we will cause Confluence to retain a reference to this class, preventing
        // the plugin from being fully un-loaded.
        moduleTracker.close();
    }
}
