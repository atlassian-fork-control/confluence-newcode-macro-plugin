package com.atlassian.confluence.ext.code.descriptor;

import java.util.Map;

/**
 *
 */
public final class ThemeDefinition {

    private String location;
    private String webResourceId;
    private Map<String, String> panelLookAndFeel;

    /**
     * Default constructor.
     *
     * @param location         The location (in the classpath) of the theme file
     * @param webResourceId    The Web Resource module to include for this theme
     * @param panelLookAndFeel The definition of the default panel look-and-feel
     */
    public ThemeDefinition(final String location, final String webResourceId, final Map<String, String> panelLookAndFeel) {
        super();
        this.location = location;
        this.webResourceId = webResourceId;
        this.panelLookAndFeel = panelLookAndFeel;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @return the webResourceId
     */
    public String getWebResourceId() {
        return webResourceId;
    }

    /**
     * @return the panelLookAndFeel
     */
    public Map<String, String> getPanelLookAndFeel() {
        return panelLookAndFeel;
    }

}
