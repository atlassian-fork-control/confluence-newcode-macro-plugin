package com.atlassian.confluence.ext.code.render;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.ext.code.config.NewcodeSettings;
import com.atlassian.confluence.ext.code.config.NewcodeSettingsManager;
import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.confluence.ext.code.languages.UnknownLanguageException;
import com.atlassian.confluence.ext.code.themes.ThemeRegistry;
import com.atlassian.confluence.ext.code.themes.UnknownThemeException;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * This class implements the rendering logic to format the content of the macro.
 */
@Component
public final class ContentFormatter {

    private static final Logger LOG = LoggerFactory.getLogger(ContentFormatter.class);

    private final PageBuilderService pageBuilderService;
    private final LanguageRegistry languageRegistry;
    private final ThemeRegistry themeRegistry;
    private final NewcodeSettingsManager newcodeSettingsManager;

    private ParameterMapper parameterMapper = new ParameterMapper();

    @Autowired
    public ContentFormatter(final ThemeRegistry themeRegistry,
                            final LanguageRegistry languageRegistry,
                            final @ComponentImport PageBuilderService pageBuilderService,
                            final NewcodeSettingsManager newcodeSettingsManager) {
        this.themeRegistry = themeRegistry;
        this.languageRegistry = languageRegistry;
        this.pageBuilderService = pageBuilderService;
        this.newcodeSettingsManager = newcodeSettingsManager;
    }

    /**
     * Format the contents inside the macro based on the parameters. This will
     * add a &lt;script&gt; tag with a specific class and type which will be
     * picked up by the JavaScript based rendering.
     *
     * @param conversionContext The rendering context.
     * @param parameters        The parameters specified by the user.
     * @param body              The contents of the macro.
     * @return The formatted content.
     * @throws InvalidValueException In case of parameter errors
     */
    public String formatContent(final ConversionContext conversionContext,
                                final Map<String, String> parameters,
                                final String body)
            throws InvalidValueException {
        boolean isExport = RenderContextOutputType.PDF.equals(conversionContext.getOutputType())
                || RenderContextOutputType.WORD.equals(conversionContext.getOutputType())
                || RenderContextOutputType.HTML_EXPORT.equals(conversionContext.getOutputType());
        boolean isMobile = "mobile".equals(conversionContext.getOutputDeviceType());

        LOG.debug("Starting rendering of content");

        Map<String, String> parametersWithDefaults = updateParametersWithDefaults(parameters);
        Map<String, String> mappedParameters = mapParameters(parametersWithDefaults,
                isExport);
        String theme = parameterMapper.getTheme(parametersWithDefaults).toLowerCase();
        String langStr = this.parameterMapper.getLanguage(parametersWithDefaults);
        verifyWithRegistry(langStr, theme);

        Language lang;
        try {
            lang = languageRegistry.getLanguage(langStr);
        } catch (UnknownLanguageException e) {
            // We've verified this language exists above, so this catch will never execute
            throw new InvalidValueException("lang");
        }

        Optional<String> customWebResource = lang.isBuiltIn() ?
                Optional.empty() :
                Optional.of(lang.getWebResource());

        String renderedContent = createRenderContent(body, mappedParameters, customWebResource);

        LOG.debug("Add web resources needed for rendering");

        if (isMobile) {
            requireMobileResources();
        }

        LOG.debug("Rendering of content finished");

        return renderedContent;
    }

    private void requireMobileResources() {
        pageBuilderService.assembler().resources().requireWebResource("confluence.macros.newcode.macro.mobile");
    }

    /**
     * Merge the defined defaults for the parameters with the supplied parameters map. The specified parameters
     * take precedence over the defaults.
     *
     * @param parameters The specified parameters.
     * @return The merged parameters
     */
    private Map<String, String> updateParametersWithDefaults(final Map<String, String> parameters) {
        Map<String, String> merged = new HashMap<>(parameters);

        //NCODE-90
        if (merged.containsKey(Parameters.PARAM_LANGUAGE)) {
            merged.put(Parameters.PARAM_LANG, merged.get(Parameters.PARAM_LANGUAGE));
        }

        // Default language fallbacks happen here.
        // - If the macro simply specified no language, fall back to the configured default.
        // - If the macro specifies a language that doesn't exist, fall back to the configured
        //      default language. (this may happen if a custom language is un-installed)
        //  - If the specified language is not registered, but it's set to "none", then *DON'T*
        //      fall back to the default.
        // (Later on, if the configured default language is not set to anything, then it will fall
        // back to the hard-coded default)
        NewcodeSettings currentSettings = newcodeSettingsManager.getCurrentSettings();
        if (!merged.containsKey(Parameters.PARAM_LANG)) {
            merged.put(Parameters.PARAM_LANG, currentSettings.getDefaultLanguage());
        } else {
            String language = merged.get(Parameters.PARAM_LANG).toLowerCase();
            if (!languageRegistry.isLanguageRegistered(language) &&
                    !merged.get(Parameters.PARAM_LANG).equals("none")) {
                merged.put(Parameters.PARAM_LANG, currentSettings.getDefaultLanguage());
            }
        }

        if (!merged.containsKey(Parameters.PARAM_THEME)) {
            merged.put(Parameters.PARAM_THEME, currentSettings.getDefaultTheme());
        }

        return merged;
    }

    /**
     * Update the parameters with the default layout values used by the theme
     *
     * @param parameters The parameters
     * @return The updated parameters
     * @throws InvalidValueException In case of parameter errors
     */
    public Map<String, String> getPanelParametersWithThemeLayout(final Map<String, String> parameters)
            throws Exception {
        Map<String, String> merged = new HashMap<>(parameters);

        Map<String, String> parametersWithDefaults = updateParametersWithDefaults(parameters);
        String theme = parameterMapper.getTheme(parametersWithDefaults).toLowerCase();

        Map<String, String> layout;
        try {
            layout = this.themeRegistry.getThemeLookAndFeel(theme);
        } catch (UnknownThemeException e) {
            // Should not occur, just checked existence of the theme
            throw new IllegalStateException("Invalid theme", e);
        }
        for (Map.Entry<String, String> entry : layout.entrySet()) {
            if (!merged.containsKey(entry.getKey()) && entry.getValue() != null) {
                merged.put(entry.getKey(), entry.getValue());
            }
        }

        return merged;
    }

    /**
     * Performs mapping to the parameters understood by the syntax highlighter
     * JavaScript and checks the values of the parameters.
     *
     * @param parameters The input parameters
     * @param isExport   Whether the user has requested an export
     * @return The mapped parameters
     * @throws InvalidValueException In case of invalid parameter values
     */
    private Map<String, String> mapParameters(
            final Map<String, String> parameters, final boolean isExport)
            throws InvalidValueException {
        Map<String, String> tmpParameters = new HashMap<>(
                parameters);
        /*
         * Handle export as a parameter
         */
        tmpParameters.put(Parameters.PARAM_EXPORT, Boolean.toString(isExport));

        LOG.debug("Perform mapping of parameters");

        /*
         * Map the parameters
         */
        return parameterMapper.mapParameters(tmpParameters);

    }

    /**
     * Verify the language and theme with the registries.
     *
     * @param lang  The specified language.
     * @param theme The specified theme.
     * @throws InvalidValueException In case of an invalid language or theme
     */
    private void verifyWithRegistry(final String lang, final String theme) throws InvalidValueException {
        LOG.debug("Check availability of language and theme");

        if (!this.languageRegistry.isLanguageRegistered(lang)) {
            throw new InvalidValueException("lang");
        }

        if (!this.themeRegistry.isThemeRegistered(theme)) {
            throw new InvalidValueException("theme");
        }
    }

    /**
     * Embeds the content in a &lt;pre&gt; tag which is required by the
     * SyntaxHighlighter library to recognize the content to render and adds the
     * correct parameters to this tag based on the macro parameters.
     *
     * @param body       The content
     * @param parameters The macro parameters
     * @return The embedded content
     */
    private String createRenderContent(final String body,
                                       final Map<String, String> parameters,
                                       final Optional<String> customWebResource) {
        StringBuilder buffer = new StringBuilder();

        buffer.append("<pre class=\"syntaxhighlighter-pre\" data-syntaxhighlighter-params=\"");

        boolean first = true;
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            if (!first) {
                buffer.append("; ");
            }
            buffer.append(entry.getKey()).append(": ").append(entry.getValue());
            first = false;
        }

        // If this macro is using a custom language, we need to pass that through to the front-end.
        customWebResource.ifPresent(resourceString -> {
            buffer.append("\" data-custom-language-resource=\"");
            buffer.append(resourceString);
        });

        buffer.append("\" data-theme=\"");
        buffer.append(parameters.get("theme"));
        buffer.append("\">");
        buffer.append(body);
        buffer.append("</pre>");

        return buffer.toString();
    }
}
