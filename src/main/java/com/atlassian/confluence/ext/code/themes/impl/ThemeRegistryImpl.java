package com.atlassian.confluence.ext.code.themes.impl;

import com.atlassian.confluence.ext.code.descriptor.DescriptorFacade;
import com.atlassian.confluence.ext.code.descriptor.ThemeDefinition;
import com.atlassian.confluence.ext.code.themes.DuplicateThemeException;
import com.atlassian.confluence.ext.code.themes.Theme;
import com.atlassian.confluence.ext.code.themes.ThemeRegistry;
import com.atlassian.confluence.ext.code.themes.UnknownThemeException;
import org.apache.commons.collections.map.CaseInsensitiveMap;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Implementation of the theme registry.
 */
@Component
public final class ThemeRegistryImpl implements ThemeRegistry, InitializingBean {

    /**
     * Hash table of all themes as registered by name. Multi-thread safe.
     */
    private CaseInsensitiveMap themes = new CaseInsensitiveMap();

    private DescriptorFacade descriptorFacade = null;

    @Autowired
    public ThemeRegistryImpl(final DescriptorFacade descriptorFacade) {
        this.descriptorFacade = descriptorFacade;
    }

    /**
     * Lists the registered themes.
     *
     * @return The registered themes
     */
    @SuppressWarnings("unchecked")
    public Collection<Theme> listThemes() throws Exception {
        Set<Theme> result = new HashSet<>();
        result.addAll(themes.values());
        return result;
    }

    /**
     * Register a theme with the registry.
     *
     * @param theme The theme to register.
     * @throws DuplicateThemeException In case of a duplicate theme
     */
    void registerTheme(final Theme theme) throws Exception {
        if (themes.get(theme.getName()) != null) {
            throw new DuplicateThemeException("newcode.theme.register.duplicate.name",
                    theme.getName());
        }
        themes.put(theme.getName(), theme);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isThemeRegistered(final String theme) {
        return themes.containsKey(theme);
    }

    /**
     * {@inheritDoc}
     */
    public String getWebResourceForTheme(final String name) throws Exception {
        Theme theme = (Theme) themes.get(name);
        if (theme == null) {
            throw new UnknownThemeException(name);
        }
        return theme.getWebResource();
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, String> getThemeLookAndFeel(String name) throws Exception {
        Theme theme = (Theme) themes.get(name);
        if (theme == null) {
            throw new UnknownThemeException(name);
        }
        return theme.getDefaultLayout();
    }

    @PostConstruct
    public void registerDefaultThemes() throws Exception {
        ThemeDefinition[] builtins = descriptorFacade.listBuiltinThemes();
        for (ThemeDefinition themeDef : builtins) {
            String location = themeDef.getLocation();
            int start = "sh/styles/shTheme".length();
            int end = location.length() - ".css".length();
            String name = location.substring(start, end);
            BuiltinTheme theme = new BuiltinTheme(name, location, themeDef.getPanelLookAndFeel());
            theme.setWebResource(themeDef.getWebResourceId());
            registerTheme(theme);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }
}
