package com.atlassian.confluence.ext.code.config;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Persistent settings for the New code macro.
 */
public final class NewcodeSettings implements Serializable {

    public static final String DEFAULT_THEME_VALUE = "Confluence";
    public static final String DEFAULT_LANGUAGE_VALUE = "Java";

    public static final String DEFAULT_THEME = "defaultTheme";
    public static final String DEFAULT_LANGUAGE = "defaultLanguage";

    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;

    private String defaultTheme;

    private String defaultLanguage;

    /**
     * Default constructor.
     */
    public NewcodeSettings() {
        super();
    }

    /**
     * @return the defaultTheme
     */
    public String getDefaultTheme() {
        return defaultTheme;
    }

    /**
     * @param defaultTheme the defaultTheme to set
     */
    public void setDefaultTheme(final String defaultTheme) {
        this.defaultTheme = defaultTheme;
    }

    /**
     * @return the defaultLanguage
     */
    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    /**
     * @param defaultLanguage the defaultLanguage to set
     */
    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    /**
     * Map from the object representation to the persistence map.
     *
     * @return The persistence map
     */
    public Map<String, Object> settingsToMap() {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put(DEFAULT_THEME, this.getDefaultTheme());
        map.put(DEFAULT_LANGUAGE, this.getDefaultLanguage());

        return map;
    }

    /**
     * Map from the persistence format (a java.util.Map) to the object representation.
     *
     * @param map The persistence map
     */
    public void mapToSettings(final Map<String, Object> map) {
        this.setDefaultTheme((String) map.get(DEFAULT_THEME));
        this.setDefaultLanguage((String) map.get(DEFAULT_LANGUAGE));
    }

}
