package com.atlassian.confluence.ext.code.themes;


/**
 * Exception which is thrown by the {@link ThemeRegistry} in case a theme
 * is registered with a duplicate name.
 */
public final class DuplicateThemeException extends Exception {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;

    private String errorMsgKey;
    private Object[] params;

    /**
     * Default constructor.
     *
     * @param errorMsgKey The exception message
     * @param params      The exception message parameters
     */
    public DuplicateThemeException(final String errorMsgKey,
                                   final Object... params) {
        this.errorMsgKey = errorMsgKey;
        this.params = params;
    }

    /**
     * @return the errorMsgKey
     */
    public String getErrorMsgKey() {
        return errorMsgKey;
    }

    /**
     * @return the params
     */
    public Object[] getParams() {
        return params;
    }

}
