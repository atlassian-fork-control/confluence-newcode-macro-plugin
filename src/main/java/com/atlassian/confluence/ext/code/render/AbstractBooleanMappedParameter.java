package com.atlassian.confluence.ext.code.render;

import java.util.Map;

/**
 * Abstract implementation of a boolean parameter for which a mapping has to be made
 * between the name used by the macro and the syntax highlighter library.
 */
public abstract class AbstractBooleanMappedParameter extends MappedParameter {

    /**
     * Default constructor.
     *
     * @param name       The name used by the macro
     * @param mappedName The name used by the syntax highlighter
     */
    public AbstractBooleanMappedParameter(final String name, final String mappedName) {
        super(name, mappedName);
    }

    /**
     * Checks and returns the value for the configured parameter.
     *
     * @param parameters The input set of parameters
     * @return The parameter value
     * @throws InvalidValueException In case the value is not valid
     */
    protected final String internalGetValue(final Map<String, String> parameters)
            throws InvalidValueException {
        String retval = super.getValue(parameters);
        if (!(null == retval || "true".equals(retval) || "false".equals(retval))) {
            throw new InvalidValueException(this.getMacroName());
        }
        return retval;
    }

}
