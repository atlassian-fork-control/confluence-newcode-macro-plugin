package com.atlassian.confluence.ext.code.actions;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.opensymphony.xwork.Action;

import java.util.List;

import static java.util.Comparator.comparing;

/**
 * Retrieves a list of all configured syntax highlighters from the {@link LanguageRegistry} and converts it to JSON
 * format for use in the macro browser.
 */
public class GetLanguagesAction extends ConfluenceActionSupport implements Beanable {

    private LanguageRegistry languageRegistry;

    public void setLanguageRegistry(LanguageRegistry languageRegistry) {
        this.languageRegistry = languageRegistry;
    }

    /**
     * Returns the content that will be JSONified.
     */
    public Object getBean() {
        final List<Language> languages = languageRegistry.listLanguages();
        // Return the installed languages in alphabetical order.
        languages.sort(comparing(Language::getFriendlyName));
        return languages;
    }

    /**
     * Simple action - the magic is in {@link #getBean()}
     */
    @Override
    public String execute() throws Exception {
        return Action.SUCCESS;
    }
}