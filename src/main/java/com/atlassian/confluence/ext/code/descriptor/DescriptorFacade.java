package com.atlassian.confluence.ext.code.descriptor;

import java.util.List;

/**
 * Facade for changing the Confluence descriptors. Delegates all calls to the
 * actual strategy compatible with the current version of Confluence.
 */
public interface DescriptorFacade {

    /**
     * List the locations of all built-in brushes.
     *
     * @return The locations of the built-in brushes
     */
    BrushDefinition[] listBuiltinBrushes();

    /**
     * List the locations of all built-in themes.
     *
     * @return The locations of the built-in themes
     */
    ThemeDefinition[] listBuiltinThemes();

    /**
     * List all localization supported in the plugin
     *
     * @return The language key of the localization
     */
    List<String> listLocalization();
}
