package com.atlassian.confluence.ext.code.render;

import com.atlassian.confluence.ext.code.config.NewcodeSettings;

import java.util.Map;

import static com.atlassian.confluence.ext.code.render.Parameters.PARAM_THEME;

/**
 * Parameter implementation for the theme parameter.
 */
public final class ThemeParameter extends MappedParameter {

    /**
     * Default constructor.
     *
     * @param name       The name used by the macro
     * @param mappedName The name used by the syntax highlighter
     */
    public ThemeParameter(final String name, final String mappedName) {
        super(name, mappedName);
    }

    /**
     * Determines the theme to use.
     *
     * @param parameters The map of parameters.
     * @return The theme to use
     */
    @Override
    public String getValue(final Map<String, String> parameters) {
        String theme = parameters.get(PARAM_THEME);
        if (theme == null) {
            theme = NewcodeSettings.DEFAULT_THEME_VALUE;
        }
        return theme;
    }

}
