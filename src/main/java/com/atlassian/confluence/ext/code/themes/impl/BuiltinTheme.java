package com.atlassian.confluence.ext.code.themes.impl;

import com.atlassian.confluence.ext.code.themes.Theme;

import java.util.Map;

/**
 * Represent themes which are builtin into this macro.
 */
public final class BuiltinTheme implements Theme {
    private String name;
    private String styleSheetUrl;
    private String webResource;
    private Map<String, String> defaultLayout;

    /**
     * Default constructor.
     *
     * @param name          The name of the theme
     * @param styleSheetUrl The path of the stylesheet
     * @param defaultLayout The default layout of the theme
     */
    public BuiltinTheme(final String name,
                        final String styleSheetUrl,
                        final Map<String, String> defaultLayout) {
        super();
        this.name = name;
        this.styleSheetUrl = styleSheetUrl;
        this.defaultLayout = defaultLayout;
    }

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    public String getStyleSheetUrl() {
        return styleSheetUrl;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isBuiltIn() {
        return true;
    }

    /**
     * @return the webResource
     */
    public String getWebResource() {
        return webResource;
    }

    /**
     * @param webResource the webResource to set
     */
    public void setWebResource(final String webResource) {
        this.webResource = webResource;
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, String> getDefaultLayout() {
        return defaultLayout;
    }

}
