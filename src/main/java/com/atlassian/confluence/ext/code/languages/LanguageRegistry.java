package com.atlassian.confluence.ext.code.languages;

import java.util.List;

/**
 * Registry for languages supported by this plugin.
 */
public interface LanguageRegistry {

    /**
     * Returns whether a certain alias is know to the system.
     *
     * @param alias The language alias
     * @return Whether the alias is know to the system
     */
    boolean isLanguageRegistered(String alias);

    Language getLanguage(String name) throws UnknownLanguageException;

    /**
     * Return all registered languages.
     *
     * @return All registered languages
     */
    List<Language> listLanguages();

    /**
     * Returns the web resource to load for this language.
     *
     * @param alias The language alias
     * @return The full id of the web resource
     * @throws UnknownLanguageException In case the alias is unknown
     */
    String getWebResourceForLanguage(String alias) throws UnknownLanguageException;

    /**
     * Register a new externally provided language with the registry.
     *
     * @param language A parsed language script (see {@link LanguageParser}).
     * @throws DuplicateLanguageException In case either the name or the alias was previously
     *                                    registered.
     */
    void addLanguage(Language language) throws DuplicateLanguageException;

    /**
     * Unregister a language previously registered with
     * {@link #addLanguage(Language)}. In case the specified language is unknown to
     * the registry (or previously unregistered), this method won't have any
     * effect. It can therefore be safely called multiple times. Also, this
     * method won't have any affect for built-in languages.
     *
     * @param name The name of the language as passed previously to
     *             registerLanguage
     */
    void unregisterLanguage(String name);
}
