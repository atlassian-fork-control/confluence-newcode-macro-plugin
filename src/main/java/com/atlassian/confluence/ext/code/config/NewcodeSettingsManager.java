package com.atlassian.confluence.ext.code.config;

import com.atlassian.confluence.ext.code.util.Constants;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;
import java.util.function.Supplier;

/**
 * The settings manager is responsible for the persistency and management of the
 * settings of the new code macro.
 */
@Component
public class NewcodeSettingsManager {

    private final SettingsManager settingsManager;
    private final Supplier<NewcodeSettings> currentSettings;

    @Autowired
    public NewcodeSettingsManager(final @ComponentImport SettingsManager settingsManager) {
        this.settingsManager = settingsManager;

        this.currentSettings = () -> {
            Serializable settings = settingsManager.getPluginSettings(Constants.PLUGIN_KEY);
            NewcodeSettings result = new NewcodeSettings();
            if (settings != null && settings instanceof Map) {
                result.mapToSettings((Map<String, Object>) settings);
            }

            return result;
        };
    }

    /**
     * Returns the current settings, fetching settings from bandana
     *
     * @return The current settings
     */
    public NewcodeSettings getCurrentSettings() {
        return currentSettings.get();
    }

    /**
     * Update the persistent settings.
     *
     * @param theme    The new theme
     * @param language The new language
     */
    public void updateSettings(final String theme, final String language) {
        NewcodeSettings newSettings = new NewcodeSettings();
        newSettings.setDefaultTheme(theme);
        newSettings.setDefaultLanguage(language);

        this.settingsManager.updatePluginSettings(Constants.PLUGIN_KEY,
                (Serializable) newSettings.settingsToMap());
    }
}
