package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.languages.DuplicateLanguageException;
import com.atlassian.confluence.ext.code.languages.InvalidLanguageException;
import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.confluence.ext.code.languages.LanguageParser;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.confluence.ext.code.languages.RegisteredLanguageInstaller;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/**
 * A helper class that can be used to generate and install new plugin artifacts for
 * SyntaxHighlighter brushes. Once the plugin is installed, it will be picked up by
 * {@link RegisteredLanguageListener} and converted into a {@link RegisteredLanguage} that is
 * stored in the {@link LanguageRegistry}.
 * <p>
 * This class is invoked in the case where an administrator configured a new custom language for
 * the plugin by just providing the necessary JavaScript - we then need to register a plugin with a
 * web-resource module to contain and serve the provided JavaScript. If a plugin containing a
 * {@link com.atlassian.confluence.ext.code.descriptor.custom.CustomCodeSyntax}
 * module is installed via some other means (eg. marketplace), then this code will not get involved.
 */
@Component
public class RegisteredLanguageInstallerImpl implements RegisteredLanguageInstaller {
    private final LanguageRegistry languageRegistry;
    private final LanguageParser languageParser;
    private final PluginController pluginController;
    private final PluginGenerator pluginGenerator;

    @Autowired
    public RegisteredLanguageInstallerImpl(final LanguageRegistry languageRegistry,
                                           final LanguageParser languageParser,
                                           final @ComponentImport PluginController pluginController,
                                           final PluginGenerator pluginGenerator) {
        this.languageRegistry = languageRegistry;
        this.languageParser = languageParser;
        this.pluginController = pluginController;
        this.pluginGenerator = pluginGenerator;
    }

    public void installLanguage(Reader reader, String friendlyName) throws InvalidLanguageException,
            DuplicateLanguageException {
        String script;
        try {
            script = IOUtils.toString(reader);
        } catch (IOException e) {
            throw new InvalidLanguageException("The language input could not be read", e);
        }

        // Parse the script first to make sure it's valid, before going to the effort of generating
        // a plugin artifact for it.
        Language language = languageParser.parseRegisteredLanguage(new StringReader(script),
                friendlyName);

        // Do a preemptive check for duplicates - while this is not going to be 100% accurate
        // (concurrency, etc), this may at least prevent the needless effort of generating a new
        // plugin artifact in some cases.
        checkForDuplicates(language);

        // Install a plugin based on the input. This relies on {@link RegisteredLanguageListener}
        // to pick up the plugin module enabled events for this new plugin and register the code
        // macro modules with the {@link LanguageRegistry}
        PluginArtifact pluginForLanguage;
        try {
            pluginForLanguage = pluginGenerator.createPluginForLanguage(language,
                    new StringReader(script));
        } catch (IOException e) {
            throw new InvalidLanguageException("The language could not be converted to a " +
                    "plugin artifact", e);
        }
        pluginController.installPlugins(pluginForLanguage);
    }

    private void checkForDuplicates(Language language) throws DuplicateLanguageException {
        if (languageRegistry.isLanguageRegistered(language.getName())) {
            throw new DuplicateLanguageException("The language " + language.getName() +
                    " is already registered.", language.getName());
        }

        for (String alias : language.getAliases()) {
            if (languageRegistry.isLanguageRegistered(alias)) {
                throw new DuplicateLanguageException("The language " + alias +
                        " is already registered.", alias);
            }
        }
    }
}
