package com.atlassian.confluence.ext.code.themes;

/**
 * Thrown in case of problems with the the language does not exist.
 */
public final class UnknownThemeException extends Exception {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;

    private String name;

    /**
     * Default constructor.
     *
     * @param name The name of the theme
     */
    public UnknownThemeException(final String name) {
        super();
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

}
