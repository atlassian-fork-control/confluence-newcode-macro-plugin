package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.languages.Language;

import java.util.Collection;

/**
 * Represent languages which are builtin into this macro.
 */
public class BuiltinLanguage implements Language {

    private String name;
    private String friendlyName;
    private Collection<String> aliases;
    private String webResource;

    /**
     * Default constructor.
     *
     * @param name    The name of the language
     * @param aliases The aliases of the language
     */
    BuiltinLanguage(final String name, final Collection<String> aliases) {
        super();
        this.name = name;
        this.aliases = aliases;
    }

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isBuiltIn() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public Collection<String> getAliases() {
        return aliases;
    }

    /**
     * @return the webResource
     */
    public String getWebResource() {
        return webResource;
    }

    /**
     * @param webResource the webResource to set
     */
    public void setWebResource(final String webResource) {
        this.webResource = webResource;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }
}
