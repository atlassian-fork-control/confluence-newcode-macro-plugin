package com.atlassian.confluence.ext.code.render;

/**
 * Contains various constants for parameters.
 */
final class Parameters {
    static final String DEFAULT_PARAMETER = "0";

    // Supported by the 1.0 version of NewCode macro
    static final String PARAM_LANG = "lang";
    static final String PARAM_LANGUAGE = "language";
    static final String PARAM_COLLAPSE = "collapse";
    static final String PARAM_FIRSTLINE = "firstline";
    static final String PARAM_LINENUMBERS = "linenumbers";

    // Added parameters
    static final String PARAM_THEME = "theme";
    static final String PARAM_EXPORTIMAGE = "exportImage";

    /**
     * Fake parameter to indicate exporting.
     */
    static final String PARAM_EXPORT = "_isExport";

    /**
     * Private constructor.
     */
    private Parameters() {
    }
}
