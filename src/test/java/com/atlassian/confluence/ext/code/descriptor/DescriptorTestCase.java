package com.atlassian.confluence.ext.code.descriptor;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Test case for the functionality to interact with Confluence plugin
 * descriptors.
 */
public final class DescriptorTestCase extends TestCase {

    @Mock
    private ConfluenceStrategy confluenceStrategy;

    private DescriptorFacade facade;

    /**
     * {@inheritDoc}
     */
    protected void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        facade = new DescriptorFacadeImpl(confluenceStrategy);
    }

    /**
     * Test listing of the brushes as declared in atlassian-plugin.xml.
     *
     * @throws Exception In case of test failures
     */
    public void testListBrushes() throws Exception {
        BrushDefinition[] expected = {new BrushDefinition("location", "webResourceId")};
        when(confluenceStrategy.listBuiltinBrushes()).thenReturn(expected);

        BrushDefinition[] actual = facade.listBuiltinBrushes();

        assertEquals(expected, actual);
    }

    /**
     * Test listing of the themes as declared in atlassian-plugin.xml.
     *
     * @throws Exception In case of test failures
     */
    public void testListThemes() throws Exception {
        ThemeDefinition[] expected = {new ThemeDefinition("location", "webResourceId", ImmutableMap.<String, String>of("one", "two"))};
        when(confluenceStrategy.listBuiltinThemes()).thenReturn(expected);

        ThemeDefinition[] actual = facade.listBuiltinThemes();

        assertEquals(expected, actual);
    }

    public void listLocalizations() throws Exception {
        List<String> expected = ImmutableList.of("one", "two");
        when(confluenceStrategy.listLocalization()).thenReturn(expected);

        List<String> actual = facade.listLocalization();

        assertEquals(expected, actual);
    }
}
