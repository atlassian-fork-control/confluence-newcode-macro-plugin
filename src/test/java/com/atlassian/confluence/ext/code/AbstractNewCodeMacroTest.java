package com.atlassian.confluence.ext.code;

import com.atlassian.confluence.ext.code.util.Constants;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.Iterator;

public class AbstractNewCodeMacroTest {
    /**
     * Create a mock plugin to test loading of brushes and themes.
     *
     * @return The plugin
     * @throws org.dom4j.DocumentException In case of an invalid mock data file
     */
    protected Plugin createMockPlugin() throws DocumentException {
        Element element = loadModuleDescriptor("atlassian-plugin.xml");
        Plugin plugin = new StaticPlugin() {

            /**
             * {@inheritDoc}
             */
            @Override
            public String getKey() {
                return Constants.PLUGIN_KEY;
            }

        };

        for (Iterator i = element.elementIterator(); i.hasNext(); ) {
            Element child = (Element) i.next();
            if (child.getName().equals("web-resource")) {
                final WebResourceModuleDescriptor descriptor = new WebResourceModuleDescriptor(null);
                descriptor.init(plugin, child);
                plugin.addModuleDescriptor(descriptor);
            }
        }
        return plugin;
    }

    /**
     * Load mock data for a module descriptor from an XML file.
     *
     * @param filename The XML file of the classpath
     * @return The element containing the module descriptor
     * @throws DocumentException In case of parse errors
     */
    protected Element loadModuleDescriptor(final String filename) throws DocumentException {
        SAXReader reader = new SAXReader();

        InputStream is = this.getClass().getClassLoader().getResourceAsStream(filename);
        Document doc = reader.read(is);
        IOUtils.closeQuietly(is);
        return doc.getRootElement();
    }
}
