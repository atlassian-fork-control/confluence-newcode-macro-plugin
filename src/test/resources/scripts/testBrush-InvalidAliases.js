/**
 * A Test SyntaxHighligher custom brush that tries to register aliases that aren't valid strings.
 **/

function Brush() {

}

Brush.prototype = new SyntaxHighlighter.Highlighter();
Brush.aliases = [function () {
    alert("Hiya, Sport!")
}];

SyntaxHighlighter.brushes.MyTest = Brush;
